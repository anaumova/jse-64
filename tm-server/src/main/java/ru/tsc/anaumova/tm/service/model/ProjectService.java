package ru.tsc.anaumova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.anaumova.tm.api.service.model.IProjectService;
import ru.tsc.anaumova.tm.enumerated.Sort;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.exception.field.EmptyDescriptionException;
import ru.tsc.anaumova.tm.exception.field.EmptyIdException;
import ru.tsc.anaumova.tm.exception.field.EmptyNameException;
import ru.tsc.anaumova.tm.exception.field.EmptyUserIdException;
import ru.tsc.anaumova.tm.exception.system.EmptyStatusException;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.repository.model.ProjectRepository;
import ru.tsc.anaumova.tm.repository.model.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Override
    protected ProjectRepository getRepository() {
        return repository;
    }

    @NotNull
    private UserRepository getUserRepository() {
        return userRepository;
    }

    @Nullable
    @Override
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setUser(getUserRepository().findById(userId).orElse(null));
        @NotNull final ProjectRepository repository = getRepository();
        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(getUserRepository().findById(userId).orElse(null));
        @NotNull final ProjectRepository repository = getRepository();
        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(getUserRepository().findById(userId).orElse(null));
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        @NotNull final ProjectRepository repository = getRepository();
        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectRepository repository = getRepository();
        return repository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectRepository repository = getRepository();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ProjectRepository repository = getRepository();
        repository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ProjectRepository repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ProjectRepository repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectRepository repository = getRepository();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectRepository repository = getRepository();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @NotNull final Project model) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ProjectRepository repository = getRepository();
        repository.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectRepository repository = getRepository();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void update(@NotNull final Project model) {
        @NotNull final ProjectRepository repository = getRepository();
        repository.save(model);
    }

    @Nullable
    @Override
    @Transactional
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ProjectRepository repository = getRepository();
        @Nullable final Project model = repository.findFirstByUserIdAndId(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public Project changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final ProjectRepository repository = getRepository();
        @Nullable final Project model = repository.findFirstByUserIdAndId(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        repository.save(model);
        return model;
    }

    @Override
    public long getCount(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ProjectRepository repository = getRepository();
        return repository.countByUserId(userId);
    }

}