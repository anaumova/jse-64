package ru.tsc.anaumova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.dto.model.TaskDto;

import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDto> {

    @NotNull
    List<TaskDto> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}