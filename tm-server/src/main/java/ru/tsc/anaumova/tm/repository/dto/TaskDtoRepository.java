package ru.tsc.anaumova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.dto.model.TaskDto;

import java.util.List;

@Repository
public interface TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> {

    @NotNull
    List<TaskDto> findAllByUserId(@NotNull String userId);

    @Nullable
    TaskDto findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull List<TaskDto> findAllByProjectIdAndUserId(@NotNull String userId, @NotNull String projectId);

    void deleteAllByUserId(@NotNull String userId);

}