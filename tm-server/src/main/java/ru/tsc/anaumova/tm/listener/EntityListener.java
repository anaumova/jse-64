package ru.tsc.anaumova.tm.listener;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.anaumova.tm.component.JmsComponent;
import ru.tsc.anaumova.tm.enumerated.EntityOperationType;

import static ru.tsc.anaumova.tm.enumerated.EntityOperationType.*;

@Component
@NoArgsConstructor
public class EntityListener implements PostUpdateEventListener, PostDeleteEventListener, PostInsertEventListener,
        PreUpdateEventListener, PreDeleteEventListener, PreInsertEventListener, PostLoadEventListener {

    @NotNull
    @Autowired
    private JmsComponent jmsComponent;

    @SneakyThrows
    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType) {
        jmsComponent.sendMessage(entity, operationType.toString());
    }

    @Override
    public void onPostDelete(PostDeleteEvent postDeleteEvent) {
        sendMessage(postDeleteEvent.getEntity(), POST_REMOVE);
    }

    @Override
    public void onPostInsert(PostInsertEvent postInsertEvent) {
        sendMessage(postInsertEvent.getEntity(), POST_PERSIST);
    }

    @Override
    public void onPostUpdate(PostUpdateEvent postUpdateEvent) {
        sendMessage(postUpdateEvent.getEntity(), POST_UPDATE);
    }

    @Override
    public void onPostLoad(PostLoadEvent postLoadEvent) {
        sendMessage(postLoadEvent.getEntity(), POST_LOAD);
    }

    @Override
    public boolean onPreDelete(PreDeleteEvent preDeleteEvent) {
        sendMessage(preDeleteEvent.getEntity(), PRE_REMOVE);
        return false;
    }

    @Override
    public boolean onPreInsert(PreInsertEvent preInsertEvent) {
        sendMessage(preInsertEvent.getEntity(), PRE_PERSIST);
        return false;
    }

    @Override
    public boolean onPreUpdate(PreUpdateEvent preUpdateEvent) {
        sendMessage(preUpdateEvent.getEntity(), PRE_UPDATE);
        return false;
    }

    @Override
    public boolean requiresPostCommitHanding(EntityPersister entityPersister) {
        return false;
    }

}