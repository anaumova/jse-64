<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>Список задач</h1>

<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap">Проект</th>
        <th width="200" nowrap="nowrap" align="left">Название</th>
        <th width="100%" align="left">Описание</th>
        <th width="150" align="center" nowrap="nowrap">Статус</th>
        <th width="100" align="center" nowrap="nowrap">Дата начала</th>
        <th width="100" align="center" nowrap="nowrap">Дата окончания</th>
        <th width="100" align="center" nowrap="nowrap">Изменить</th>
        <th width="100" align="center" nowrap="nowrap">Удалить</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:forEach var="project" items="${projects}">
                    <c:if test="${project.id == task.projectId}">
                        <c:out value="${project.name}"/>
                    </c:if>
                </c:forEach>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td>
                <c:out value="${task.status.displayName}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateBegin}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateEnd}"/>
            </td>
            <td align="center">
                <a href="/task/edit/${task.id}/">Изменить</a>
            </td>
            <td align="center">
                <a href="/task/delete/${task.id}/">Удалить</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/task/create" style="margin-top: 20px">
    <button>Создать</button>
</form>

<jsp:include page="../include/_footer.jsp"/>