package ru.tsc.anaumova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.model.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TaskRepository {

    @NotNull
    private final Map<String, Task> tasks = new HashMap<>();

    {
        add("task 1");
        add("task 2");
        add("task 3");
    }

    public void add(@NotNull final String name) {
        final Task task = new Task(name);
        tasks.put(task.getId(), task);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    @NotNull
    public List<Task> findAll() {
        return new ArrayList<>(tasks.values());
    }

    @NotNull
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

}